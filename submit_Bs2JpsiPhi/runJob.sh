#!/bin/bash

JobID=$1
count=0
inputFile=""
while read line; do 
    if [[ $count == $JobID ]]; then 
        inputFile=${line}
        break; 
    fi
    count=$((count+1));
done < Bs2JpsiPhiMuMu_2017_MagUp_listPFN.txt
echo "INPUT FILE: ${line}"

myPATH="/home/LHCB-T3/dmanuzzi/B2HH/tests_bug2018/MCmisalign"
mkdir -p "${myPATH}/submit_Bs2JpsiPhi/output/${JobID}"
cd "${myPATH}/submit_Bs2JpsiPhi/output/${JobID}"
echo "from GaudiConf import IOHelper" > inputs.py
echo "IOHelper().inputFiles(['${inputFile}'], clear = True)" >> inputs.py

CMTCONFIG="x86_64-centos7-gcc9-opt"
BINARY_TAG="x86_64-centos7-gcc9-opt"

${myPATH}/DaVinciDev/run gaudirun.py ${myPATH}/submit_Bs2JpsiPhi/option_Refitter_tuple.py ./inputs.py

   
