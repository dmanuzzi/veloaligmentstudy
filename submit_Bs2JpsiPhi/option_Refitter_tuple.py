from Configurables import LoKi__VoidFilter as VoidFilter
from Configurables import TrackMonitor, TrackVertexMonitor
from Configurables import DaVinci
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, TupleSelection, PrintSelection, FilterSelection
from Configurables import GaudiSequencer, CopyParticles, TESCheck, TrackContainerCopy, ParticleRefitter
from TrackFitter.ConfiguredFitters import ConfiguredEventFitter
from Configurables import (
    BTaggingTool,
    DaVinci,
    GaudiSequencer,
    MCDecayTreeTuple,
    MCTupleToolP2VV, FilterDesktop
)
from Configurables import TrackStateInitAlg
from TrackFitter.ConfiguredFitters import ConfiguredEventFitter

from Gaudi.Configuration import *

refitPVs = True
refit_tracks = False;

stream_name = "AllStreams";
set_cluster_prefix = True;
use_state_init = True;
if set_cluster_prefix:
    # Direct the hit managers to get their clusters from /Event/StreamName/Raw/... 
    cluster_prefix = "/Event/{}/".format(stream_name)

    from Configurables import (OTRawBankDecoder, PatSeedFit, Tf__OTHitCreator, FastVeloHitManager,
                                Tf__DefaultVeloPhiHitManager, Tf__DefaultVeloRHitManager, Tf__PatVeloFitLHCbIDs,
                                Tf__PatVeloRHitManager, Tf__PatVeloPhiHitManager, PatVeloTTHybridFit)

    ToolSvc().addTool(OTRawBankDecoder())
    ToolSvc().addTool(PatSeedFit())
    ToolSvc().addTool(Tf__OTHitCreator("OTHitCreator") )
    ToolSvc().addTool(FastVeloHitManager("FastVeloHitManager"))
    ToolSvc().addTool(Tf__DefaultVeloPhiHitManager("DefaultVeloPhiHitManager"))
    ToolSvc().addTool(Tf__DefaultVeloRHitManager("DefaultVeloRHitManager"))
    ToolSvc().addTool(PatVeloTTHybridFit());

    ToolSvc().OTRawBankDecoder.RootInTES = cluster_prefix
    ToolSvc().PatSeedFit.RootInTES = cluster_prefix
    ToolSvc().OTHitCreator.RootInTES = cluster_prefix
    ToolSvc().FastVeloHitManager.RootInTES = cluster_prefix
    ToolSvc().DefaultVeloPhiHitManager.RootInTES = cluster_prefix
    ToolSvc().DefaultVeloRHitManager.RootInTES = cluster_prefix

    ToolSvc().addTool(Tf__PatVeloRHitManager("PatVeloRHitManager"))
    ToolSvc().addTool(Tf__PatVeloPhiHitManager("PatVeloPhiHitManager"))
    ToolSvc().addTool(Tf__PatVeloFitLHCbIDs("FitVelo"));
    ToolSvc().PatVeloRHitManager.RootInTES=cluster_prefix
    ToolSvc().PatVeloPhiHitManager.RootInTES=cluster_prefix
    ToolSvc().FitVelo.RootInTES=cluster_prefix
    ToolSvc().PatVeloTTHybridFit.RootInTES=cluster_prefix;

def refit_tracks_and_particles(inputselections, RootInTES, 
                                tracklocations = ['Rec/Track/Best'],
                                do_particle_refitting=False):
    '''Configure refitting of tracks and particles from the given Selections. Copy the originals before
    refitting. Returns the refitting sequence and a list of Selections that point to the originals (non-refitted).
    The given input Selections will then use the refitted tracks & particles.'''
    from Configurables import FastVeloFitLHCbIDs, TrackStateInitTool, TrackMasterExtrapolator, SimplifiedMaterialLocator

    if not isinstance(inputselections, (tuple, list)):
        inputselections = [inputselections]
    if not isinstance(tracklocations, (tuple, list)):
        tracklocations = [tracklocations]
    refitseq = GaudiSequencer('RefitSeq', IgnoreFilterPassed = True)
    copyseq = GaudiSequencer('CopySeq', IgnoreFilterPassed = True)
    refitseq.Members.append(copyseq)
    originals = []
    # Refit the tracks.
    for loc in tracklocations:
        trackrefitseq = GaudiSequencer('TrackRefit:' + loc)
        refitseq.Members.append(trackrefitseq)
        canonical_location = loc.replace("/", "_");
        
        if "Best" in loc and use_state_init:
            stateinitalg = TrackStateInitAlg("StateInitAlg: "+ canonical_location,
                                        TrackLocation=loc)

            stateinitalg.addTool(TrackStateInitTool, name="StateInitTool")
            stateinitalg.StateInitTool.VeloFitterName = "FastVeloFitLHCbIDs"
            stateinitalg.StateInitTool.UseFastMomentumEstimate = True
            stateinitalg.StateInitTool.addTool(TrackMasterExtrapolator, "Extrapolator")
            stateinitalg.StateInitTool.Extrapolator.addTool(  SimplifiedMaterialLocator, name="MaterialLocator")
            stateinitalg.StateInitTool.addTool( FastVeloFitLHCbIDs, name="FastVeloFitLHCbIDs")
            stateinitalg.StateInitTool.FastVeloFitLHCbIDs.UseKalmanFit = True
            stateinitalg.StateInitTool.RootInTES = RootInTES;
            if set_cluster_prefix:
                stateinitalg.StateInitTool.RootInTES = cluster_prefix;

            trackrefitseq.Members += [ stateinitalg ];

        # Refit the original tracks in place.
        evtfitter = ConfiguredEventFitter('RefitTracks_' + canonical_location, loc, SimplifiedGeometry = True,
                                          LiteClusters=True, MSRossiAndGreisen=True)

        if set_cluster_prefix:
            ### Set the MeasurementProvider paths
            import importlib
            confs = importlib.import_module('Configurables')
            nonstandard_names = {
            'OT' : 'OTMeasurementProvider',
            'Muon' : 'MuonMeasurementProvider',
            }
            for det in ['VeloPhi', 'TT', 'IT', 'OT', 'VeloR', 'Muon']:
                provider_name = det + 'MeasurementProvider'
                provider_type_name = nonstandard_names.get(det, 'MeasurementProviderT_MeasurementProviderTypes__' + det + '_')
                provider_type = getattr(confs, provider_type_name)
                provider = evtfitter.Fitter.MeasProvider.addTool(provider_type(provider_name))
                provider.RootInTES = RootInTES #or wherever your clusters sit at

        if set_cluster_prefix:
            evtfitter.RootInTES=cluster_prefix

        trackrefitseq.Members += [ evtfitter ]


    if do_particle_refitting:
        partrefitseq = GaudiSequencer('ParticleRefitSeq', IgnoreFilterPassed = False)
        for inputsel in inputselections:
            partrefitter = ParticleRefitter('RefitParticles:' + inputsel.name(),
                                            Inputs = [inputsel.outputLocation()],
                                            FitTool="LoKi::VertexFitter")
            partrefitseq.Members.append(partrefitter)
    
        # Add the particle refitters to the sequence.
        refitseq.Members.append(partrefitseq)

    # Return the refitting sequence and Selections for the original, non-refitted particles.
    return refitseq, originals


### First, let's DoD the rec track best into dimuon
# (acutally we could've just put the raw event from Dimuon into the main TES, but OK)
from Configurables import DataOnDemandSvc
dod = DataOnDemandSvc()
from Configurables import Gaudi__DataLink as Link
rawEvt1 = Link ( 'LinkBestTracks',    What   =  '/Event/Rec/Track/Best' ,   Target = '/Event/{}/Rec/Track/Best'.format(stream_name) )
dod.AlgMap [ rawEvt1  . Target ] = rawEvt1

# and also the FittedHLT1VeloTracks
rawEvt2 = Link ( 'LinkVELOTracks',   What   =  '/Event/Rec/Track/FittedHLT1VeloTracks' ,  Target = '/Event/{}/Rec/Track/FittedHLT1VeloTracks'.format(stream_name) )
dod.AlgMap [ rawEvt2  . Target ] = rawEvt2

# and also the PVs
rawEvt3 = Link ( 'LinkVertices',  What   =  '/Event/Rec/Vertex/Primary' ,  Target = '/Event/{}/Rec/Vertex/Primary'.format(stream_name) )
dod.AlgMap [ rawEvt3  . Target ] = rawEvt3

# Refit pvs
my_particles = AutomaticData(Location="/Event/{}/Phys/BetaSBs2JpsiPhiDetachedLine/Particles".format(stream_name))

# let's do some refitting
refitsequence, original_particles = refit_tracks_and_particles( my_particles, 
                                                            RootInTES="/Event/{}".format(stream_name),
                                                            tracklocations=[
                                                                "Rec/Track/Best",
                                                                "Rec/Track/FittedHLT1VeloTracks"
                                                            ],
                                                            do_particle_refitting= True)

from Configurables import LoKi__Hybrid__TupleTool
LoKi_nHits = LoKi__Hybrid__TupleTool("LoKi_nHits")
LoKi_nHits.Preambulo = ["from LoKiTracks.decorators import *"]
LoKi_nHits.Variables = {
"ITClusters": "switch(ISBASIC, TRFUN(TrIDC('isIT')), -1)",
"OTClusters": "switch(ISBASIC, TRFUN(TrIDC('isOT')), -1)",
"TTClusters": "switch(ISBASIC, TRFUN(TrIDC('isTT')), -1)",
"VeloClusters":"switch(ISBASIC, TRFUN(TrIDC('isVelo')), -1)"}

is_mc=True
tools = [
    'TupleToolANNPID',
    'TupleToolEventInfo',
    'TupleToolKinematic',
    'TupleToolL0Calo',
    'TupleToolL0Data',
    'TupleToolPid',
    'TupleToolPrimaries',
    'TupleToolPropertime',
    'TupleToolTISTOS'
]
if is_mc:
    tools += [
        'TupleToolMCBackgroundInfo'
    ]

refit_pvs=True
name='Bs2JpsiPhi_Signal'
if refit_pvs:
    tuple_input = FilterSelection(
        'CheckRefittedPVs_{}'.format(name),
        [my_particles],
        Code='BPVVALID()',
        ReFitPVs=True
    )


my_tuple = TupleSelection ( 'Bs2JpsiPhi_Signal_Tuple' ,  
                            #[my_particles], 
                            [tuple_input], 
                            ToolList = tools,
                            Decay = "[[B_s0]cc -> ^(J/psi(1S) -> ^mu+ ^mu-) ^(phi(1020) -> ^K+ ^K-)]CC",
                            ReFitPVs=True)

my_tuple.addTupleTool( LoKi_nHits );
my_tuple.UseP2PVRelations = True
my_tuple.IgnoreP2PVFromInputLocations = True

dtt=my_tuple.algorithm()

L0_LINES = [
    'L0DiMuonDecision',
    'L0GlobalDecision',
    'L0MuonDecision'
]
HLT1_LINES = [
    'Hlt1DiMuonHighMassDecision',
    'Hlt1DiMuonLowMassDecision',
    'Hlt1DiMuonNoL0Decision',
    'Hlt1GlobalDecision',
    'Hlt1L0AnyDecision',
    'Hlt1SingleMuonHighPTDecision',
    'Hlt1TrackMVADecision',
    'Hlt1TrackMVALooseDecision',
    'Hlt1TrackMuonDecision',
    'Hlt1TrackMuonMVADecision',
    'Hlt1TwoTrackMVADecision',
    'Hlt1TwoTrackMVALooseDecision'
]
HLT2_LINES = [
    'Hlt2DiMuonDecision',
    'Hlt2DiMuonDetachedDecision',
    'Hlt2DiMuonDetachedHeavyDecision',
    'Hlt2DiMuonDetachedJPsiDecision',
    'Hlt2DiMuonDetachedPsi2SDecision',
    'Hlt2DiMuonJPsiDecision',
    'Hlt2SingleMuonDecision',
    'Hlt2SingleMuonHighPTDecision',
    'Hlt2SingleMuonLowPTDecision',
    'Hlt2SingleMuonRareDecision',
    'Hlt2SingleMuonVHighPTDecision',
    'Hlt2Topo2BodyDecision',
    'Hlt2Topo3BodyDecision',
    'Hlt2Topo4BodyDecision',
    'Hlt2TopoMu2BodyDecision',
    'Hlt2TopoMu3BodyDecision',
    'Hlt2TopoMu4BodyDecision',
    'Hlt2TopoMuMu2BodyDecision',
    'Hlt2TopoMuMu3BodyDecision',
    'Hlt2TopoMuMu4BodyDecision'
]
TRIGGER_LINES = L0_LINES + HLT1_LINES + HLT2_LINES

branches={
    'B'       : 'B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (phi(1020) ->  K+  K-)',
    'Jpsi'    : 'B_s0 -> ^(J/psi(1S) ->  mu+  mu-)  (phi(1020) ->  K+  K-)',
    'muplus'  : 'B_s0 ->  (J/psi(1S) -> ^mu+  mu-)  (phi(1020) ->  K+  K-)',
    'muminus' : 'B_s0 ->  (J/psi(1S) ->  mu+ ^mu-)  (phi(1020) ->  K+  K-)',
    'X'       : 'B_s0 ->  (J/psi(1S) ->  mu+  mu-) ^(phi(1020) ->  K+  K-)',
    'hplus'   : 'B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (phi(1020) -> ^K+  K-)',
    'hminus'  : 'B_s0 ->  (J/psi(1S) ->  mu+  mu-)  (phi(1020) ->  K+ ^K-)'
}
dtt.addBranches(branches)
geom = dtt.addTupleTool('TupleToolGeometry')
geom.RefitPVs = True
geom.Verbose = True

reco_stats = dtt.addTupleTool('TupleToolRecoStats')
reco_stats.Verbose = True

track_inf = dtt.addTupleTool('TupleToolTrackInfo')
track_inf.Verbose = True

track_pos = dtt.addTupleTool('TupleToolTrackPosition')
track_pos.Z = 7500.0

trig_tool = dtt.addTupleTool('TupleToolTrigger')
trig_tool.Verbose = True
trig_tool.TriggerList = TRIGGER_LINES
trig_tool.OutputLevel = 6

b_tistos = dtt.B.addTupleTool('TupleToolTISTOS')
b_tistos.Verbose = True
b_tistos.TriggerList = TRIGGER_LINES

jpsi_tistos = dtt.Jpsi.addTupleTool('TupleToolTISTOS')
jpsi_tistos.Verbose = True
jpsi_tistos.TriggerList = TRIGGER_LINES


LoKi_B = dtt.B.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_B')
LoKi_B.Variables = {
        'ETA': 'ETA',
        'DOCA': 'DOCA(1,2)',
        'Y': 'Y',
        'LV01': 'LV01',
        'LV02': 'LV02',
        'LOKI_FDCHI2': 'BPVVDCHI2',
        'LOKI_FDS': 'BPVDLS',
        'LOKI_DIRA': 'BPVDIRA',
        'LOKI_DTF_CTAU': 'DTF_CTAU(0, True)',
        'LOKI_DTF_CTAUS': 'DTF_CTAUSIGNIFICANCE(0, True)',
        'LOKI_DTF_CHI2NDOF': 'DTF_CHI2NDOF(True)',
        'LOKI_DTF_CTAUERR': 'DTF_CTAUERR(0, True)',
        'LOKI_MASS_JpsiConstr': 'DTF_FUN(M, True, \'J/psi(1S)\')',
        'LOKI_DTF_VCHI2NDOF': 'DTF_FUN(VFASPF(VCHI2/VDOF), True)',
        'muplus_DTF_PE'    : "DTF_FUN ( CHILD(1, CHILD(1,E)),True, \'J/psi(1S)\' )",
        'muplus_DTF_PX'    : "DTF_FUN ( CHILD(1, CHILD(1,PX)),True, \'J/psi(1S)\' )",
        'muplus_DTF_PY'    : "DTF_FUN ( CHILD(1, CHILD(1,PY)),True, \'J/psi(1S)\' )",
        'muplus_DTF_PZ'    : "DTF_FUN ( CHILD(1, CHILD(1,PZ)),True, \'J/psi(1S)\' )",
        'muminus_DTF_PE'    : "DTF_FUN ( CHILD(1, CHILD(2,E)),True, \'J/psi(1S)\' )",
        'muminus_DTF_PX'    : "DTF_FUN ( CHILD(1, CHILD(2,PX)),True, \'J/psi(1S)\' )",
        'muminus_DTF_PY'    : "DTF_FUN ( CHILD(1, CHILD(2,PY)),True, \'J/psi(1S)\' )",
        'muminus_DTF_PZ'    : "DTF_FUN ( CHILD(1, CHILD(2,PZ)),True, \'J/psi(1S)\' )"
}

LoKi_Jpsi = dtt.Jpsi.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_Jpsi')
LoKi_Jpsi.Variables = {
    'ETA': 'ETA',
    'Y': 'Y',
    'LV01': 'LV01',
    'LV02': 'LV02',
    'LOKI_FDCHI2': 'BPVVDCHI2',
    'LOKI_FDS': 'BPVDLS',
    'LOKI_DIRA': 'BPVDIRA',
}

LoKi_muonp = dtt.muplus.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_muonp')
LoKi_muonm = dtt.muminus.addTupleTool('LoKi::Hybrid::TupleTool/LoKi_muonm')
LoKi_muonp.Variables = {
    'LOKI_ETA': 'ETA',
    'LOKI_Y': 'Y'
}

LoKi_muonm.Variables = {
    'LOKI_ETA': 'ETA',
    'LOKI_Y': 'Y'
}
decay_head = {
    'Bu': 'B+',
    'Bd': 'B0',
    'Bs': 'B_s0'
}['Bs']
# refit with constraints
fitter_params = [
    ('ConstJpsi', True, ['J/psi(1S)']),
    ('ConstJpsiNoPV', False, ['J/psi(1S)']),
    ('ConstBJpsi', True, [decay_head, 'J/psi(1S)']),
    ('ConstOnlyPV', True, [])
]

for fitter_name, constrain_to_pv, daughter_constraints in fitter_params:
    fitter = dtt.B.addTupleTool('TupleToolDecayTreeFitter/{}'.format(fitter_name))
    fitter.Verbose = True
    fitter.UpdateDaughters = True
    fitter.constrainToOriginVertex = constrain_to_pv
    fitter.daughtersToConstrain = daughter_constraints

'''
# Don't get the tagging decision on MC microDSTs
is_mdst=False
from FlavourTagging.Tunings import applyTuning as applyFTTuning
with_ft = not (is_mc and is_mdst)
if with_ft:
    btag = dtt.addTupleTool('TupleToolTagging')
    # Take the tagging decision from microDSTs, otherwise recompute it
    if is_mdst:
        btag.UseFTfromDST = True
    else:
        btag.Verbose = True
        btag.AddMVAFeatureInfo = True
 
        btagtool = btag.addTool(BTaggingTool, name='MyBTaggingTool_{}'.format(name))
 
        ## peilian: does this tunning_version work for all years?
        applyFTTuning(btagtool, tuning_version='Summer2017Optimisation_v4_Run2')
        btag.TaggingToolName = btagtool.getFullName()
'''


LoKi_EvtTuple = dtt.addTupleTool('LoKi::Hybrid::EvtTupleTool/LoKi_EvtTuple')
LoKi_EvtTuple.VOID_Variables = {
    'LoKi_nPVs': "CONTAINS('Rec/Vertex/Primary')",
    'LoKi_nSpdMult': "CONTAINS('Raw/Spd/Digits')",
    'LoKi_nVeloClusters': "CONTAINS('Raw/Velo/Clusters')",
    'LoKi_nVeloLiteClusters': "CONTAINS('Raw/Velo/LiteClusters')",
    'LoKi_nITClusters': "CONTAINS('Raw/IT/Clusters')",
    'LoKi_nTTClusters': "CONTAINS('Raw/TT/Clusters')",
    'LoKi_nOThits': "CONTAINS('Raw/OT/Times')"
}

fitter_configs = [
    # B -> KplusPiMuMu
    ('B2KpPiJpsi', {'Beauty -> Meson (phi(1020) -> ^K+ X-)': 'pi+'}),
    # B -> KminusPiMuMu
    ('B2KmPiJpsi', {'Beauty -> Meson (phi(1020) -> X+ ^K-)': 'pi-'}),
    # Lb -> pKMuMu (Kplus)
    ('pKMuMuKplus', {'Beauty -> Meson (phi(1020) -> ^K+ X-)': 'p+'}),
    # Lb -> pKMuMu (Kminus)
    ('pKMuMuKminus', {'Beauty -> Meson (phi(1020) -> X+ ^K-)': 'p~-'})
]
for fitter_name, substitutions in fitter_configs:
    fitter = dtt.B.addTupleTool('TupleToolDecayTreeFitter/{}'.format(fitter_name))
    fitter.Verbose = True
    fitter.Substitutions = substitutions
    fitter.daughtersToConstrain = ['J/psi(1S)']
    fitter.constrainToOriginVertex = True

""" Miriam commented this for the older DV version """

docas = dtt.B.addTupleTool('TupleToolDOCA')
doca_name, location1, location2 = zip(
    *[[
          'hplus_hminus',
          'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->  ^K+   K-)',
          'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->   K+  ^K-)'
      ],
      [
          'muplus_muminus',
          'B_s0 ->  (J/psi(1S) ->  ^mu+   mu-)  (phi(1020) ->   K+   K-)',
          'B_s0 ->  (J/psi(1S) ->   mu+  ^mu-)  (phi(1020) ->   K+   K-)'
      ],
      [
          'hplus_muplus',
          'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->  ^K+   K-)',
          'B_s0 ->  (J/psi(1S) ->  ^mu+   mu-)  (phi(1020) ->   K+   K-)'
      ],
      [
          'hplus_muminus',
          'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->  ^K+   K-)',
          'B_s0 ->  (J/psi(1S) ->   mu+  ^mu-)  (phi(1020) ->   K+   K-)'
      ],
      [
          'hminus_muplus',
          'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->   K+  ^K-)',
          'B_s0 ->  (J/psi(1S) ->  ^mu+   mu-)  (phi(1020) ->   K+   K-)'
      ],
      [
          'hminus_muminus',
          'B_s0 ->  (J/psi(1S) ->   mu+   mu-)  (phi(1020) ->   K+  ^K-)',
          'B_s0 ->  (J/psi(1S) ->   mu+  ^mu-)  (phi(1020) ->   K+   K-)'
      ]])

docas.Name = list(doca_name)
docas.Location1 = list(location1)
docas.Location2 = list(location2)


if is_mc:
    mctruth = dtt.addTupleTool('TupleToolMCTruth')
    mctruth.ToolList = [
        'MCTupleToolKinematic',
        'MCTupleToolHierarchy',
        'MCTupleToolPID'
    ]

useBestTracksForPV=True
'''
if refitPVs:
    from Configurables import LoKi__PVReFitter

    ### Maybe I should instead use PVReFitterAlg?
    ## (and overwrite the old TES loc)
    #mypvrefitter = LoKi__PVReFitter();
    #mypvrefitter.DeltaChi2 = 1.
    #mypvrefitter.DeltaDistance = 100.
    #mypvrefitter.FullReFit = True

    ### ??? THIS DOESN"T WORK?
    #my_tuple.PVReFitters.update( {"": "LoKi::PVReFitter"} )

    #mypvrefitter.OutputLevel = 1;
    #my_tuple.ReFitPVs=True
    from Configurables import TrackPVRefitter
    if useBestTracksForPV:
        pvrefitter = TrackPVRefitter(PVLocation="Rec/Vertex/Primary", TrackLocation="Rec/Track/Best", KeepOriginalTrackIfMissing=True)
        pvrefitter.Enable = False
        refitsequence.Members += [ pvrefitter ]

    else:
        pvrefitter = TrackPVRefitter(PVLocation="Rec/Vertex/Primary")
        pvrefitter.Enable = False
        refitsequence.Members += [ pvrefitter ]
'''




def mc_tuple_maker(name, decay, branches):
    """Return a configured MCDecayTreeTuple instance."""
    dtt = MCDecayTreeTuple('{}_MCTuple'.format(name))
    dtt.Decay = decay
    dtt.TupleName = 'MCTuple'
    dtt.ToolList += [
        'MCTupleToolKinematic',
        'TupleToolEventInfo',
        'MCTupleToolHierarchy',
        'MCTupleToolPrimaries',
        'MCTupleToolPID'
    ]
    dtt.addBranches(branches)

    return dtt


# make sure that we do not process events without the raw bank
GaudiSequencer("DaVinciUserSequence").IgnoreFilterPassed = False

from Configurables import LoKi__HDRFilter as StripFilter
my_stripping_filter = StripFilter( 'StrippingFilterForBetas',
                            Code="HLT_PASS('StrippingBetaSBs2JpsiPhiDetachedLineDecision')",
                            Location="/Event/Strip/Phys/DecReports" )



from Configurables import DaVinci
DaVinci().InputType       = 'DST'
DaVinci().Turbo           = False
DaVinci().DataType        = "2017"
DaVinci().Simulation      = True 

is_mc = DaVinci().Simulation
mc_tuples = []
if is_mc:
    mc_tuples.append(mc_tuple_maker(
        'Bs2JpsiPhi_Signal',
        '[[B_s0]cc ==> ^(J/psi(1S) ==> ^mu+ ^mu-) ^(phi(1020) ==> ^K+ ^K-)]CC',
        {
            'B'       : '[[B_s0]cc ==>  (J/psi(1S) ==>  mu+  mu-)  (phi(1020) ==>  K+  K-)]CC',
            'Jpsi'    : '[[B_s0]cc ==> ^(J/psi(1S) ==>  mu+  mu-)  (phi(1020) ==>  K+  K-)]CC',
            'muplus'  : '[[B_s0]cc ==>  (J/psi(1S) ==> ^mu+  mu-)  (phi(1020) ==>  K+  K-)]CC',
            'muminus' : '[[B_s0]cc ==>  (J/psi(1S) ==>  mu+ ^mu-)  (phi(1020) ==>  K+  K-)]CC',
            'X'       : '[[B_s0]cc ==>  (J/psi(1S) ==>  mu+  mu-) ^(phi(1020) ==>  K+  K-)]CC',
            'hplus'   : '[[B_s0]cc ==>  (J/psi(1S) ==>  mu+  mu-)  (phi(1020) ==> ^K+  K-)]CC',
            'hminus'  : '[[B_s0]cc ==>  (J/psi(1S) ==>  mu+  mu-)  (phi(1020) ==>  K+ ^K-)]CC'
        }
    ))

DaVinci().EventPreFilters = [ my_stripping_filter ] 
DaVinci().UserAlgorithms  += [ refitsequence ]+ [ my_tuple ] + mc_tuples  
DaVinci().EvtMax          = 10
DaVinci().Lumi            = False 
DaVinci().RootInTES       = "/Event/AllStream/"
DaVinci().TupleFile       = "./DVTuples_Tx100.root" 
DaVinci().PrintFreq       = 1 
DaVinci().DDDBtag = 'dddb-20170721-3'
DaVinci().CondDBtag =  'sim-20190430-1-vc-mu100'

from Configurables import CondDB
CondDB().addLayer( "/home/LHCB-T3/dmanuzzi/B2HH/tests_bug2018/MCmisalign/submit_Bs2JpsiPhi/SIMCOND")


# from GaudiConf import IOHelper
# IOHelper().inputFiles([
#      #'root://xrootd.echo.stfc.ac.uk/lhcb:prod/lhcb/MC/2017/ALLSTREAMS.DST/00092537/0000/00092537_00000163_7.AllStreams.dst'
# #'root://x509up_u71634@lhcbxrootd-kit.gridka.de//pnfs/gridka.de/lhcb/LHCb-Disk/lhcb/MC/2017/ALLSTREAMS.DST/00092547/0000/00092547_00000217_7.AllStreams.dst'
# #'root://proxy@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/MC/2017/ALLSTREAMS.DST/00092537/0000/00092537_00000135_7.AllStreams.dst'
#         ], clear = True)

#from Gaudi.Configuration import FileCatalog
#FileCatalog().Catalogs = [ 'xmlcatalog_file:stocazzo4.xml' ]
